package com.infotmt.resume.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(){
        super("User Not Found Exception");
    }
}
