package com.infotmt.resume.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class FieldType {
    @Id
    private long id;
    private int order;
    private String name;
    private String keyName;

    //set on custom fieldTypes
    private boolean custom;
    private long userId;

    //set on simple fieldTypes
    private boolean simple;
    //ref
    private long inputTypeCode;
    private String inputType;
    //set on non simple fieldTypes
    private boolean multi;
    @DBRef
    private List<FieldType> fieldTypes;
    private Map<String, Object> extra;

    public FieldType(long id,String name,long inputTypeCode,boolean simple){
        this.id=id;
        this.name=name;
        this.inputTypeCode=inputTypeCode;
        this.simple=simple;
    }

    public FieldType(long id, String name, boolean multi, List<FieldType> fieldTypes) {
        this.id = id;
        this.name = name;
        this.multi = multi;
        this.fieldTypes = fieldTypes;
    }
}
