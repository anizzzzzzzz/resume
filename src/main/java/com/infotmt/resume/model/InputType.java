package com.infotmt.resume.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InputType {
    @Id
    private long id;
    private String type; //Text, email Address, gmail address

    public InputType(String type){
        this.type=type;
    }

}

