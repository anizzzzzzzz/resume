package com.infotmt.resume.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Job {
    @Id
    private long id;
    private String title;
    private String description;
    private long userId;

    @DBRef
    private List<FieldType> fieldTypes;

}
