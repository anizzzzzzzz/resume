package com.infotmt.resume.model.user;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Set;

@Data
public class Role {
    @Id
    private Long id;
    private String name;
}
