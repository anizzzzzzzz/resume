package com.infotmt.resume.model.user;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Set;

@Data
public class User {
    @Id
    private Long id;
    private String username;
    private String password;
    @DBRef
    private Set<Role> roles;
}
