package com.infotmt.resume.repository;

import com.infotmt.resume.model.FieldType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FieldTypeRepository extends MongoRepository<FieldType,Long> {
    Page<FieldType> findAllByUserIdOrUserId(Pageable pageable, long id,long id1);
}
