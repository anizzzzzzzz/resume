package com.infotmt.resume.repository;

import com.infotmt.resume.model.InputType;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InputTypeRepository extends MongoRepository<InputType,Long> {
}
