package com.infotmt.resume.repository;

import com.infotmt.resume.model.Job;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JobRepository extends MongoRepository<Job,Long> {

    Job findAllByTitle(String title);

    void deleteByTitle(String title);
}
