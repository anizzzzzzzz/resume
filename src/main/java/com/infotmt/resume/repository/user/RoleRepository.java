package com.infotmt.resume.repository.user;

import com.infotmt.resume.model.user.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role,Long> {
}
