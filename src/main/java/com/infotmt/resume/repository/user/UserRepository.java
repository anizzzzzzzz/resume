package com.infotmt.resume.repository.user;

import com.infotmt.resume.model.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User,Long> {
    User findByUsernameAndPassword(String username,String password);

    User findByUsername(String username);
}
