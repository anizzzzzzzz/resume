package com.infotmt.resume.restcontroller;

import com.infotmt.resume.model.InputType;
import com.infotmt.resume.service.InputTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/job/inputType")
public class InputTypeRestController {
    private final InputTypeService inputTypeService;

    @Autowired
    public InputTypeRestController(InputTypeService inputTypeService) {
        this.inputTypeService = inputTypeService;
    }

    @PostMapping
    public List<InputType> save(@RequestBody List<InputType> inputTypes){
        return inputTypeService.save(inputTypes);
    }

    @GetMapping
    public List<InputType> findAll(){
        return inputTypeService.findAll();
    }


}
