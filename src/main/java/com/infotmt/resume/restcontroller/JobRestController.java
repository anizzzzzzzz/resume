package com.infotmt.resume.restcontroller;

import com.infotmt.resume.dto.FieldTypeDto;
import com.infotmt.resume.model.FieldType;
import com.infotmt.resume.model.InputType;
import com.infotmt.resume.model.Job;
import com.infotmt.resume.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/admin/job")
public class JobRestController {
    private final JobService jobService;

    @Autowired
    public JobRestController(JobService jobService) {
        this.jobService = jobService;
    }

    @GetMapping
    public Page<Job> findAll(Pageable pageable) {

        return jobService.findAll(pageable);
    }

    @GetMapping("/{id}")
    public Job findOne(@PathVariable long id) {
        return jobService.findOne(id);
    }



    @PostMapping
    public Job save(@RequestBody Job job){
        return jobService.saveCustom(job);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        if(jobService.findOne(id).getFieldTypes()!=null){
            jobService.deleteField(jobService.findOne(id).getFieldTypes());
        }
        jobService.delete(id);
    }

    //------------FieldType------------------

    @GetMapping("/{id}/field")
    public List<FieldType> findAllFieldsOfJob(@PathVariable long id){
        return jobService.findOne(id).getFieldTypes();
    }

    @DeleteMapping("/{id}/field/{fid}")
    public void deleteField(@PathVariable long id) {
        jobService.deleteField(id);
    }

    @GetMapping("/field/{userId}")
    public Page<FieldType> findAllFieldsByUserId(@PathVariable long userId, Pageable pageable){
        return jobService.findAllByUserId(pageable,userId);
    }

    @PostMapping("/field")
    public List<FieldType> saveField(@RequestBody List<FieldType> fieldTypes){
        return jobService.save(fieldTypes);
    }

    @GetMapping("/field")
    public Page<FieldType> findAllFieldType(Pageable pageable){
        return jobService.findAllFieldType(pageable);
    }

}
