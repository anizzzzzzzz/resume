package com.infotmt.resume.restcontroller;

import com.infotmt.resume.dto.MessageDto;
import com.infotmt.resume.model.user.Role;
import com.infotmt.resume.model.user.User;
import com.infotmt.resume.repository.user.RoleRepository;
import com.infotmt.resume.service.user.SecurityService;
import com.infotmt.resume.service.user.UserService;
import com.infotmt.resume.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @PostMapping("/registration")
    public MessageDto registerUser(@RequestBody User user, BindingResult bindingResult){
        userValidator.validate(user, bindingResult);

        /*if (bindingResult.hasErrors()) {
            return "registration";
        }*/

        boolean check =userService.save(user);
//        securityService.login(userForm.getUsername(),userForm.getPassword());

        if(check){
            return new MessageDto("Successfully registered..");
        }
        else{
            return new MessageDto("Sorry.. username has already been taken..");
        }
    }

    @PostMapping("/login")
    public MessageDto login(@RequestBody User user){
        boolean check=false;
        User user1=userService.findByUsername(user.getUsername());

        if((user1!=null) && bCryptPasswordEncoder.matches(user.getPassword(),user1.getPassword())){
            check=securityService.login(user.getUsername(), user.getPassword());

            if(check){
                return new MessageDto(user.getUsername()+" Congrats you are logged in");
            }
            else{
                return new MessageDto("Password not correct");
            }
        }
        else{
            return new MessageDto(" Username or password Not correct");
        }
    }

    @PostMapping("/roles")
    public List<Role> addRoles(@RequestBody List<Role> list){
        return roleRepository.save(list);
    }

    @GetMapping("/roles")
    public List<Role> findAll(){
        return roleRepository.findAll();
    }
}
