package com.infotmt.resume.service;

import com.infotmt.resume.model.InputType;

import java.util.List;

public interface InputTypeService {
    List<InputType> save(List<InputType> inputTypes);

    List<InputType> findAll();

    InputType findOne(long id);
}
