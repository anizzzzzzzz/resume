package com.infotmt.resume.service;

import com.infotmt.resume.model.FieldType;
import com.infotmt.resume.model.InputType;
import com.infotmt.resume.model.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JobService {
    Page<Job> findAll(Pageable pageable);

    Job findOne(long id);

    Job saveCustom(Job job);

    void delete(long id);

    //--------FieldType-------------------

    List<FieldType> save(List<FieldType> fieldType);

    void deleteField(long id);

    void deleteField(List<FieldType> fieldType);

    Page<FieldType> findAllByUserId(Pageable pageable,long id);

    Page<FieldType> findAllFieldType(Pageable pageable);
}
