package com.infotmt.resume.service.user;

public interface SecurityService {
    String findLoggedInUsername();

    boolean login(String username, String password);
}
