package com.infotmt.resume.service.user;

import com.infotmt.resume.model.user.User;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserService {
    boolean save(User user);

    User findByUsernameAndPassword(String username,String password);

    User findByUsername(String username);

}
