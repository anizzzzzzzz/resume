package com.infotmt.resume.serviceImpl;

import com.infotmt.resume.model.InputType;
import com.infotmt.resume.repository.InputTypeRepository;
import com.infotmt.resume.service.InputTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InputTypeServiceImpl implements InputTypeService {
    private final InputTypeRepository inputTypeRepository;

    private final CounterService counterService;

    @Autowired
    public InputTypeServiceImpl(CounterService counterService, InputTypeRepository inputTypeRepository) {
        this.counterService = counterService;
        this.inputTypeRepository = inputTypeRepository;
    }

    @Override
    public List<InputType> save(List<InputType> inputTypes) {
        for (int i=0;i<inputTypes.size();i++){
            inputTypes.get(i).setId(counterService.getNextSequence("inputType"));
        }
        return inputTypeRepository.save(inputTypes);
    }

    @Override
    public List<InputType> findAll() {
        return inputTypeRepository.findAll();
    }

    @Override
    public InputType findOne(long id) {
        return inputTypeRepository.findOne(id);
    }
}
