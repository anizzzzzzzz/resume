package com.infotmt.resume.serviceImpl;

import com.infotmt.resume.model.FieldType;
import com.infotmt.resume.model.Job;
import com.infotmt.resume.repository.FieldTypeRepository;
import com.infotmt.resume.repository.InputTypeRepository;
import com.infotmt.resume.repository.JobRepository;
import com.infotmt.resume.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JobServiceImpl implements JobService{
    private final JobRepository jobRepository;

    private final FieldTypeRepository fieldRepository;

    private final InputTypeRepository inputTypeRepository;

    private final CounterService counterService;

    @Autowired
    public JobServiceImpl(CounterService counterService, JobRepository jobRepository, FieldTypeRepository fieldRepository, InputTypeRepository inputTypeRepository) {
        this.counterService = counterService;
        this.jobRepository = jobRepository;
        this.fieldRepository = fieldRepository;
        this.inputTypeRepository = inputTypeRepository;
    }

    @Override
    public Page<Job> findAll(Pageable pageable) {
        return jobRepository.findAll(pageable);
    }

    @Override
    public Job findOne(long id) {
        return jobRepository.findOne(id);
    }

    @Override
    public Job saveCustom(Job job) {
        if(job.getFieldTypes()!=null) {
            for (int i = 0; i < job.getFieldTypes().size(); i++) {
                job.getFieldTypes().get(i).setId(counterService.getNextSequence("fieldType"));
                job.getFieldTypes().get(i).setOrder(i + 1);
                job.getFieldTypes().get(i).setUserId(job.getUserId());
                job.getFieldTypes().get(i).setCustom(true);

                if (job.getFieldTypes().get(i).getInputTypeCode() != 0) {
                    //----------For Setting InputType-----------------
                    job.getFieldTypes().get(i).setInputType(inputTypeRepository
                            .findOne(job.getFieldTypes().get(i).getInputTypeCode()).getType());

                    if (job.getFieldTypes().get(i).getFieldTypes() == null) {
                        job.getFieldTypes().get(i).setSimple(true);
                    }

                }
                //----For nested FieldType List-----------
                else if (job.getFieldTypes().get(i).getFieldTypes() != null) {

                    for (int j = 0; j < job.getFieldTypes().get(i).getFieldTypes().size(); j++) {
                        job.getFieldTypes().get(i).getFieldTypes().get(j).setUserId(job.getUserId());
                        job.getFieldTypes().get(i).getFieldTypes().get(j).setOrder(j + 1);
                        job.getFieldTypes().get(i).getFieldTypes().get(j).setCustom(true);

                        if (job.getFieldTypes().get(i).getFieldTypes().get(j).getExtra() == null) {
                            job.getFieldTypes().get(i).getFieldTypes().get(j).setSimple(true);
                        }

                        //----------For Setting InputType of Nested List-----------------
                        job.getFieldTypes().get(i).getFieldTypes().get(j).setInputType(inputTypeRepository
                                .findOne(job.getFieldTypes()
                                        .get(i).getFieldTypes().get(j).getInputTypeCode()).getType());

                        job.getFieldTypes().get(i).getFieldTypes().get(j).setId(counterService.getNextSequence("fieldType"));

                        fieldRepository.save(job.getFieldTypes().get(i).getFieldTypes().get(j));
                    }
                }


            }
            fieldRepository.save(job.getFieldTypes());
        }

        job.setId(counterService.getNextSequence("job"));
        return jobRepository.save(job);
    }

    @Override
    public void delete(long id) {
        jobRepository.delete(id);
    }


    //--------------FieldType-----------------------------

    @Override
    public List<FieldType> save(List<FieldType> fieldType) {
        for(int i=0;i<fieldType.size();i++){
            fieldType.get(i).setId(counterService.getNextSequence("fieldType"));

            if(fieldType.get(i).getInputTypeCode()!=0){
                //----------For Setting InputType-----------------
                fieldType.get(i).setInputType(inputTypeRepository
                        .findOne(fieldType.get(i).getInputTypeCode()).getType());

                if(fieldType.get(i).getFieldTypes()==null) {
                    fieldType.get(i).setSimple(true);
                }

            }
            else if(fieldType.get(i).getFieldTypes()!=null){

                for(int j=0;j<fieldType.get(i).getFieldTypes().size();j++){
                    if(fieldType.get(i).getFieldTypes().get(j).getExtra()==null){
                        fieldType.get(i).getFieldTypes().get(j).setSimple(true);
                    }

                    //----------For Setting InputType of Nested List-----------------
                    fieldType.get(i).getFieldTypes().get(j).setInputType(inputTypeRepository
                            .findOne(fieldType
                                    .get(i).getFieldTypes().get(j).getInputTypeCode()).getType());

                    fieldType.get(i).getFieldTypes().get(j).setId(counterService.getNextSequence("fieldType"));

                    fieldRepository.save(fieldType.get(i).getFieldTypes().get(j));
                }
            }
        }
        return fieldRepository.save(fieldType);
    }

    @Override
    public void deleteField(long id) {
        fieldRepository.delete(id);
    }

    @Override
    public void deleteField(List<FieldType> fieldType) {
        fieldRepository.delete(fieldType);
    }

    @Override
    public Page<FieldType> findAllByUserId(Pageable pageable,long id) {
        return fieldRepository.findAllByUserIdOrUserId(pageable,id,0);
    }

    @Override
    public Page<FieldType> findAllFieldType(Pageable pageable) {
        return fieldRepository.findAll(pageable);
    }
}
