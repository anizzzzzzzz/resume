package com.infotmt.resume.serviceImpl.user;

import com.infotmt.resume.model.user.User;
import com.infotmt.resume.repository.user.RoleRepository;
import com.infotmt.resume.repository.user.UserRepository;
import com.infotmt.resume.service.user.UserService;
import com.infotmt.resume.serviceImpl.CounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CounterService counterService;

    @Override
    public boolean save(User user) {
        boolean check=false;
        if(userRepository.findByUsername(user.getUsername())==null) {
            user.setId((long)counterService.getNextSequence("user"));
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setRoles(new HashSet<>(roleRepository.findAll()));
            userRepository.save(user);
            check=true;
        }
        return check;
    }

    @Override
    public User findByUsernameAndPassword(String username,String password) {
        return userRepository.findByUsernameAndPassword(username,password);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
