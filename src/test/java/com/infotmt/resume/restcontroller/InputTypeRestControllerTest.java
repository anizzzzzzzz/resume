package com.infotmt.resume.restcontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infotmt.resume.model.InputType;
import com.infotmt.resume.service.InputTypeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.core.Is.is;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {InputTypeRestController.class})
public class InputTypeRestControllerTest {
    private MockMvc mockMvc;

    private List<InputType> inputTypes;

    @Mock
    private InputTypeService inputTypeService;

    @InjectMocks
    private InputTypeRestController inputTypeRestController;

    @SuppressWarnings("Duplicates")
    @Before
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(inputTypeRestController)
                .build();
        inputTypes = new ArrayList<>();
        inputTypes.add(new InputType(1,"Text"));
        inputTypes.add(new InputType(2,"Number"));
        inputTypes.add(new InputType(3,"Date"));
        inputTypes.add(new InputType(4,"Password"));
    }

    @Test
    public void save() throws Exception {
        Mockito.when(inputTypeRestController.save(inputTypes)).thenReturn(inputTypes);

        mockMvc.perform(post("/admin/job/inputType")
            .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(inputTypes)))
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    public void findAll() throws Exception {
        Mockito.when(inputTypeRestController.findAll()).thenReturn(inputTypes);

        mockMvc.perform(get("/admin/job/inputType"))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id",is((int)inputTypes.get(0).getId())))
                .andExpect(jsonPath("$.[0].type",is(inputTypes.get(0).getType())))
                .andExpect(jsonPath("$.[1].id",is((int)inputTypes.get(1).getId())))
                .andExpect(jsonPath("$.[1].type",is(inputTypes.get(1).getType())));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}