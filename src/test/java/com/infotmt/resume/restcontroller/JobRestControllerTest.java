package com.infotmt.resume.restcontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.infotmt.resume.model.FieldType;
import com.infotmt.resume.model.Job;
import com.infotmt.resume.service.JobService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {JobRestController.class})
public class JobRestControllerTest {
    private MockMvc mockMvc;

    private List<Job> jobs;
    private List<FieldType> parentFieldTypes;
    private List<FieldType> childrenFieldTypes;

    @Mock
    private JobService jobService;

    @InjectMocks
    private JobRestController jobRestController;

    @Before
    public void init(){
        mockMvc= MockMvcBuilders.standaloneSetup(jobRestController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();

        childrenFieldTypes=new ArrayList<>();
        childrenFieldTypes.add(new FieldType(1,"University",1,true));
        childrenFieldTypes.add(new FieldType(1,"Degree",1,true));

        parentFieldTypes =new ArrayList<>();
        parentFieldTypes.add(new FieldType(1,"Name",1,true));
        parentFieldTypes.add(new FieldType(2,"Address",1,true));
        parentFieldTypes.add(new FieldType(3,"Phone",1,true));
        parentFieldTypes.add(new FieldType(4,"Education",true, childrenFieldTypes));

        jobs=new ArrayList<>();
        jobs.add(new Job(1,"Java","This is Java",1, parentFieldTypes));
        jobs.add(new Job(2,"Front End","THis is front end",1, parentFieldTypes));
    }

    @Test
    public void findAllJob() throws Exception {
        Page<Job> page = new PageImpl<>(jobs);
        when(jobRestController.findAll(any(Pageable.class))).thenReturn(page);

        mockMvc.perform(get("/admin/job"))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                //FOr Job 1
                .andExpect(jsonPath("$.content[0].id", is((int) jobs.get(0).getId())))
                .andExpect(jsonPath("$.content[0].title",is(jobs.get(0).getTitle())))
                .andExpect(jsonPath("$.content[0].description",is(jobs.get(0).getDescription())))
//                    .andExpect(jsonPath("$.content[0].fieldTypes",is(asJsonString(jobs.get(0).getFieldTypes()).replaceAll("",""))))
                .andExpect(jsonPath("$.content[0].fieldTypes[0].name",is(jobs.get(0).getFieldTypes().get(0).getName())))
                .andExpect(jsonPath("$.content[0].fieldTypes[0].inputTypeCode",is((int)jobs.get(0).getFieldTypes()
                        .get(0).getInputTypeCode())))
                .andExpect(jsonPath("$.content[0].fieldTypes[0].simple",is(jobs.get(0).getFieldTypes().get(0).isSimple())))
                .andExpect(jsonPath("$.content[0].fieldTypes[1].name",is(jobs.get(0).getFieldTypes().get(1).getName())))
                .andExpect(jsonPath("$.content[0].fieldTypes[1].inputTypeCode",is((int)jobs.get(0).getFieldTypes()
                        .get(1).getInputTypeCode())))
                .andExpect(jsonPath("$.content[0].fieldTypes[1].simple",is(jobs.get(0).getFieldTypes().get(1).isSimple())))
                .andExpect(jsonPath("$.content[0].fieldTypes[2].name",is(jobs.get(0).getFieldTypes().get(2).getName())))
                .andExpect(jsonPath("$.content[0].fieldTypes[2].inputTypeCode",is((int)jobs.get(0).getFieldTypes()
                        .get(2).getInputTypeCode())))
                .andExpect(jsonPath("$.content[0].fieldTypes[2].simple",is(jobs.get(0).getFieldTypes().get(2).isSimple())))

                .andExpect(jsonPath("$.content[0].fieldTypes[3].fieldTypes[0].name",is(jobs.get(0).getFieldTypes().get(3)
                        .getFieldTypes().get(0).getName())))
                .andExpect(jsonPath("$.content[0].fieldTypes[3].fieldTypes[0].inputTypeCode",is((int)jobs.get(0).getFieldTypes()
                        .get(3).getFieldTypes().get(0).getInputTypeCode())))
                .andExpect(jsonPath("$.content[0].fieldTypes[3].fieldTypes[0].simple",is(jobs.get(0).getFieldTypes().get(3)
                        .getFieldTypes().get(0).isSimple())))
                .andExpect(jsonPath("$.content[0].fieldTypes[3].fieldTypes[1].name",is(jobs.get(0).getFieldTypes().get(3)
                        .getFieldTypes().get(1).getName())))
                .andExpect(jsonPath("$.content[0].fieldTypes[3].fieldTypes[1].inputTypeCode",is((int)jobs.get(0).getFieldTypes()
                        .get(3).getFieldTypes().get(1).getInputTypeCode())))
                .andExpect(jsonPath("$.content[0].fieldTypes[3].fieldTypes[1].simple",is(jobs.get(0).getFieldTypes().get(3)
                        .getFieldTypes().get(1).isSimple())))
                //FOr Job 2
                .andExpect(jsonPath("$.content[1].id", is((int) jobs.get(1).getId())))
                .andExpect(jsonPath("$.content[1].title",is(jobs.get(1).getTitle())))
                .andExpect(jsonPath("$.content[1].description",is(jobs.get(1).getDescription())))
//                        .andExpect(jsonPath("$.content[1].fieldTypes",is(asJsonString(jobs.get(1).getFieldTypes()))))
              .andExpect(jsonPath("$.content[1].fieldTypes[0].name",is(jobs.get(1).getFieldTypes().get(0).getName())))
              .andExpect(jsonPath("$.content[1].fieldTypes[0].inputTypeCode",is((int)jobs.get(1).getFieldTypes()
                      .get(0).getInputTypeCode())))
              .andExpect(jsonPath("$.content[1].fieldTypes[0].simple",is(jobs.get(1).getFieldTypes().get(0).isSimple())))
              .andExpect(jsonPath("$.content[1].fieldTypes[1].name",is(jobs.get(1).getFieldTypes().get(1).getName())))
              .andExpect(jsonPath("$.content[1].fieldTypes[1].inputTypeCode",is((int)jobs.get(1).getFieldTypes()
                      .get(1).getInputTypeCode())))
              .andExpect(jsonPath("$.content[1].fieldTypes[1].simple",is(jobs.get(1).getFieldTypes().get(1).isSimple())))
              .andExpect(jsonPath("$.content[1].fieldTypes[2].name",is(jobs.get(1).getFieldTypes().get(2).getName())))
              .andExpect(jsonPath("$.content[1].fieldTypes[2].inputTypeCode",is((int)jobs.get(1).getFieldTypes()
                      .get(2).getInputTypeCode())))
              .andExpect(jsonPath("$.content[1].fieldTypes[2].simple",is(jobs.get(1).getFieldTypes().get(2).isSimple())))

                .andExpect(jsonPath("$.content[1].fieldTypes[3].fieldTypes[0].name",is(jobs.get(1).getFieldTypes().get(3)
                      .getFieldTypes().get(0).getName())))
                .andExpect(jsonPath("$.content[1].fieldTypes[3].fieldTypes[0].inputTypeCode",is((int)jobs.get(1).getFieldTypes()
                        .get(3).getFieldTypes().get(0).getInputTypeCode())))
                .andExpect(jsonPath("$.content[1].fieldTypes[3].fieldTypes[0].simple",is(jobs.get(1).getFieldTypes().get(3)
                        .getFieldTypes().get(0).isSimple())))
                .andExpect(jsonPath("$.content[1].fieldTypes[3].fieldTypes[1].name",is(jobs.get(1).getFieldTypes().get(3)
                        .getFieldTypes().get(1).getName())))
                .andExpect(jsonPath("$.content[1].fieldTypes[3].fieldTypes[1].inputTypeCode",is((int)jobs.get(1).getFieldTypes()
                        .get(3).getFieldTypes().get(1).getInputTypeCode())))
                .andExpect(jsonPath("$.content[0].fieldTypes[3].fieldTypes[1].simple",is(jobs.get(1).getFieldTypes().get(3)
                        .getFieldTypes().get(1).isSimple())));
    }

    @Test
    public void findOneJob() throws Exception{
        Job job=jobs.get(0);

        when(jobRestController.findOne(any(Long.class))).thenReturn(job);

        mockMvc.perform(get("/admin/job/1"))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", is((int) job.getId())))
                .andExpect(jsonPath("title",is(job.getTitle())))
                .andExpect(jsonPath("description",is(job.getDescription())))
//                    .andExpect(jsonPath("fieldTypes",is(asJsonString(job.getFieldTypes()).replaceAll("",""))))
                .andExpect(jsonPath("fieldTypes[0].name",is(job.getFieldTypes().get(0).getName())))
                .andExpect(jsonPath("fieldTypes[0].inputTypeCode",is((int)job.getFieldTypes()
                        .get(0).getInputTypeCode())))
                .andExpect(jsonPath("fieldTypes[0].simple",is(job.getFieldTypes().get(0).isSimple())))
                .andExpect(jsonPath("fieldTypes[1].name",is(job.getFieldTypes().get(1).getName())))
                .andExpect(jsonPath("fieldTypes[1].inputTypeCode",is((int)job.getFieldTypes()
                        .get(1).getInputTypeCode())))
                .andExpect(jsonPath("fieldTypes[1].simple",is(job.getFieldTypes().get(1).isSimple())))
                .andExpect(jsonPath("fieldTypes[2].name",is(job.getFieldTypes().get(2).getName())))
                .andExpect(jsonPath("fieldTypes[2].inputTypeCode",is((int)job.getFieldTypes()
                        .get(2).getInputTypeCode())))
                .andExpect(jsonPath("fieldTypes[2].simple",is(job.getFieldTypes().get(2).isSimple())))

                .andExpect(jsonPath("fieldTypes[3].fieldTypes[0].name",is(job.getFieldTypes().get(3)
                        .getFieldTypes().get(0).getName())))
                .andExpect(jsonPath("fieldTypes[3].fieldTypes[0].inputTypeCode",is((int)job.getFieldTypes()
                        .get(3).getFieldTypes().get(0).getInputTypeCode())))
                .andExpect(jsonPath("fieldTypes[3].fieldTypes[0].simple",is(job.getFieldTypes().get(3)
                        .getFieldTypes().get(0).isSimple())))
                .andExpect(jsonPath("fieldTypes[3].fieldTypes[1].name",is(job.getFieldTypes().get(3)
                        .getFieldTypes().get(1).getName())))
                .andExpect(jsonPath("fieldTypes[3].fieldTypes[1].inputTypeCode",is((int)job.getFieldTypes()
                        .get(3).getFieldTypes().get(1).getInputTypeCode())))
                .andExpect(jsonPath("fieldTypes[3].fieldTypes[1].simple",is(job.getFieldTypes().get(3)
                        .getFieldTypes().get(1).isSimple())));
    }

    @Test
    public void saveJob() throws  Exception{
        Job job=jobs.get(0);
        when(jobRestController.save(any(Job.class))).thenReturn(job);

        mockMvc.perform(post("/admin/job")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(job)))
                .andExpect(status().isOk())
                .andReturn();
    }


    @Test
    public void deleteJob() throws  Exception{
        Job job=jobs.get(0);

        when(jobRestController.findOne(job.getId())).thenReturn(job);
        jobRestController=Mockito.mock(JobRestController.class);
        Mockito.doNothing().when(jobRestController).delete(job.getId());

        mockMvc.perform(delete("/admin/job/{id}",job.getId()))
                .andExpect(status().isOk())
                .andReturn();

        verify(jobRestController, never()).delete(job.getId());
        verifyNoMoreInteractions(jobRestController);
    }

    //------------FieldType------------------
    //TODO
    /*@Test
    public void findAllFieldsOfJob() throws Exception{
        List<FieldType> fieldType=jobs.get(0).getFieldTypes();
        jobRestController=mock(JobRestController.class);

        when(jobRestController.findAllFieldsOfJob(any(Long.class))).thenReturn(fieldType);


        mockMvc.perform(get("/admin/job/0/field"))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.0.id",is(fieldType.get(0).getId())));
    }*/

    @Test
    public void findAllFieldType() throws Exception{
        Page<FieldType> fieldTypes=new PageImpl<>(parentFieldTypes);

        when(jobRestController.findAllFieldType(any(Pageable.class))).thenReturn(fieldTypes);

        mockMvc.perform(get("/admin/job/field"))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                //For FieldType 1
                .andExpect(jsonPath("$.content[0].id",is((int)parentFieldTypes.get(0).getId())))
                .andExpect(jsonPath("$.content[0].name",is(parentFieldTypes.get(0).getName())))
                .andExpect(jsonPath("$.content[0].inputTypeCode",is((int)parentFieldTypes.get(0).getInputTypeCode())))
                .andExpect(jsonPath("$.content[0].inputType",is(parentFieldTypes.get(0).getInputType())))
                .andExpect(jsonPath("$.content[0].extra",is(parentFieldTypes.get(0).getExtra())))
                .andExpect(jsonPath("$.content[0].order",is(parentFieldTypes.get(0).getOrder())))
                .andExpect(jsonPath("$.content[0].userId",is((int)parentFieldTypes.get(0).getUserId())))
                //For FieldType 2
                .andExpect(jsonPath("$.content[1].id",is((int)parentFieldTypes.get(1).getId())))
                .andExpect(jsonPath("$.content[1].name",is(parentFieldTypes.get(1).getName())))
                .andExpect(jsonPath("$.content[1].inputTypeCode",is((int)parentFieldTypes.get(1).getInputTypeCode())))
                .andExpect(jsonPath("$.content[1].inputType",is(parentFieldTypes.get(1).getInputType())))
                .andExpect(jsonPath("$.content[1].extra",is(parentFieldTypes.get(1).getExtra())))
                .andExpect(jsonPath("$.content[1].order",is(parentFieldTypes.get(1).getOrder())))
                .andExpect(jsonPath("$.content[1].userId",is((int)parentFieldTypes.get(1).getUserId())));
    }

    @Test
    public void saveField() throws Exception{
        when(jobRestController.saveField(any(List.class))).thenReturn(parentFieldTypes);

        mockMvc.perform(post("/admin/job/field")
            .contentType(APPLICATION_JSON_UTF8)
            .content(asJsonString(parentFieldTypes)))
                .andExpect(status().isOk())
                .andReturn();
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}