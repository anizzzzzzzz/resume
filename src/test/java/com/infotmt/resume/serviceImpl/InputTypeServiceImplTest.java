package com.infotmt.resume.serviceImpl;

import com.infotmt.resume.model.InputType;
import com.infotmt.resume.repository.InputTypeRepository;
import jdk.internal.util.xml.impl.Input;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {InputTypeServiceImpl.class})
public class InputTypeServiceImplTest {
    @Mock
    private CounterService counterService;

    @Mock
    private InputTypeRepository inputTypeRepository;

    @InjectMocks
    private InputTypeServiceImpl inputTypeService;

    private List<InputType> inputTypes;

    @SuppressWarnings("Duplicates")
    @Before
    public void init(){
        inputTypes = new ArrayList<>();
        inputTypes.add(new InputType("Text"));
        inputTypes.add(new InputType("Number"));
        inputTypes.add(new InputType("Date"));
        inputTypes.add(new InputType("Password"));
    }

    @Test
    public void findAll(){
        when(inputTypeRepository.findAll()).thenReturn(inputTypes);
        List<InputType> in=inputTypeService.findAll();
        assertNotNull(in);
        assertEquals(in, inputTypes);

    }

    @Test
    public void save(){
        when(inputTypeRepository.save(any(List.class))).thenReturn(inputTypes);

        List<InputType> list=inputTypeService.save(inputTypes);
        assertNotNull(list);
        assertEquals(inputTypes,list);
    }

}